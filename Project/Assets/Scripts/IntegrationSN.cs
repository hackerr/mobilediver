﻿using UnityEngine;
using System.Collections;

public class IntegrationSN : MonoBehaviour
{

      public string ApplicationLink = "https://play.google.com/store/apps/details?id=com.Exlain.Paranoia_Fox";    // Cсылка на ваше приложение, которым пользователь будет делиться
      public string Title = "Paranoia Fox | Лисичка Паранойя"; // Заголовок поста
      public string Description = "Помоги Лисичке Паранойе не попасть под чары грибов и странной воды, и собери как можно больше яблок!"; // Содержание поста
      public string ImageUrl = "http://5665tm.ucoz.ru/1234/ico.png"; // Ссылка на логотип игры

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void ShareVK()
    { 
       Application.OpenURL("http://vk.com/share.php?url=" + ApplicationLink
                                                            + "&title=" + Title
                                                            + "&description=" + Description
                                                            + "&image=" + ImageUrl);   
    }
 }