﻿using UnityEngine;
using System.Collections;

public class LvlController : MonoBehaviour {

	// Use this for initialization

    public bool gameOver;
    public GameObject gameOverUI;
    public GameObject winUI;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player" && gameOver)
        {
            gameOverUI.SetActive(true);
        }
        else 
            if (coll.tag == "Player" && gameOver == false)
            {
                winUI.SetActive(true);
            }
    }

}
