﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DiverController : MonoBehaviour {

    public GameObject diver;
    public GameObject cep;

    public GameObject levelFrom, levelTo,rope,firstpart;

    private List<Transform> levelObjects;
    private bool rotate = false;

	// Use this for initialization
	void Start () {
        levelObjects = new List<Transform>();
        for (int i = 0; i < levelFrom.transform.childCount; i++)
        {
            levelObjects.Add(levelFrom.transform.GetChild(i));
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A))
        {
                Change();
                levelFrom.transform.Rotate(Vector3.forward * Time.deltaTime*10);
                ChangeBack();
        }
        if (Input.GetKey(KeyCode.D))
        {
            Change();

            levelFrom.transform.Rotate(-Vector3.forward * Time.deltaTime*10);
            ChangeBack();
        }
        
	}

    void Change()
    {
        for (int i = 0; i < levelObjects.Count; i++)
        {
            levelObjects[i].parent = levelTo.transform;
        }
        firstpart.transform.parent = levelTo.transform;
        levelFrom.transform.position = levelTo.transform.position;
        for (int i = 0; i < levelObjects.Count; i++)
        {
            levelObjects[i].parent = levelFrom.transform;
            if (levelObjects[i].name != "First")
                levelObjects[i].localScale = new Vector3(1, 1, 1);
            else
                levelObjects[i].localScale = new Vector3(0.0178125f, 0.04163675f, 1);
        }
        firstpart.transform.parent = rope.transform;
        rotate = true;
    }

    void ChangeBack()
    {
        for (int i = 0; i < levelObjects.Count; i++)
        {
            levelObjects[i].parent = levelFrom.transform;
            if (levelObjects[i].name != "First")
                levelObjects[i].localScale = new Vector3(1, 1, 1);
            else
                levelObjects[i].localScale = new Vector3(0.0178125f, 0.04163675f, 1);
        }
        firstpart.transform.parent = rope.transform;
    }
}
