﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RopeController : MonoBehaviour {

    public GameObject partOfRope;
    public GameObject first, last;
    public List<Rigidbody2D> parts;

    private float timer = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            if (timer <= 0)
            {
                AddPart();
                timer = 0.2f;
            }
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
	}

    private void AddPart()
    {
        Vector2 spawnPoint;
        if(parts.Count >0)
        {
            spawnPoint = new Vector2(parts[parts.Count - 1].transform.position.x, parts[parts.Count - 1].transform.position.y - 0.2201f);
        }
        else
        {
            spawnPoint = new Vector2(first.transform.position.x, first.transform.position.y -0.2201f);
        }

        last.GetComponent<DistanceJoint2D>().enabled = false;
        last.GetComponent<Rigidbody2D>().isKinematic = true;
        GameObject inst = GameObject.Instantiate(partOfRope, spawnPoint, new Quaternion(0, 0, 0, 0)) as GameObject;
        inst.transform.parent = transform;
        DistanceJoint2D joint = inst.GetComponent<DistanceJoint2D>();
        joint.connectedBody = parts[parts.Count - 1];
        last.GetComponent<DistanceJoint2D>().connectedBody = inst.GetComponent<Rigidbody2D>();
        last.GetComponent<DistanceJoint2D>().enabled = true;
        last.GetComponent<Rigidbody2D>().isKinematic = false;
        parts.Add(inst.GetComponent<Rigidbody2D>());
    }

}
